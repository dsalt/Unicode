# unicode - A basic unicode listing tool

## Build

To build, a C compiler like GCC or Clang is needed. It can simply be compiled
with

```
gcc -O2 -o unicode unicode.c    # for GCC
clang -O2 -o unicode unicode.c  # for Clang
```

Once installed, the UCD needs to be installed locally on your system. It's
probably packaged – in Debian and similar distributions, you should
install the package “unicode-data” if it's not already present:

```
sudo apt install unicode-data
```

Otherwise, the UCD can be found here: https://www.unicode.org/Public/UCD/latest/ucd/
Download `UCD.zip` (or `UnicodeData.txt`, which is all that this program
needs) and either place the content

* in `/usr/local/share/unicode` (you need to create the `unicode` directory if
  it doesn't exist); or
* anywhere else, but don't forget you have pass `-D /path/to/UnicodeData.txt`
  for every invocation.

Once this is done, test it by running `./unicode 20`, and you should get:

```
U+0020: SPACE
  Category: Separator, Space
  Text direction: Whitespace
```

If you get the message `Unable to open default UCD file`, that means that the `unicode`
directory is not placed in the correct path or that the file couldn't be
opened for some reason.

If you get the message `Unable to open UCD file`, you've used `-D` with the
wrong file name or something's wrong with the file; in either case, that
message will be followed by error text describing the failure.

## Install

To install globally, run the following commands:

```
sudo cp unicode /usr/local/bin/unicode
sudo chown root:root /usr/local/bin/unicode
sudo chmod 755 /usr/local/bin/unicode
```

or

```
sudo install -m 755 -o root -g root unicode /usr/local/bin
```

Note that it's strongly recommended that you place the UCD in the global
directory if you plan to install globally, so everyone on the system can use it
without any additional work.
