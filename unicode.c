/*
    unicode - A basic unicode listing tool
    Copyright (C) 2023  Gustaf Alhäll

    This program is free software: you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation, either version 3 of the License, or
    (at your option) any later version.

    This program is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License
    along with this program.  If not, see <https://www.gnu.org/licenses/>.
*/

#include <stdlib.h>
#include <stdbool.h>
#include <string.h>
#include <stdio.h>
#include <assert.h>
#include <ctype.h>
#include <unistd.h>
#include <errno.h>

static char const *ucd_file;

static char const *const default_ucd[] = {
    "/usr/local/share/unicode/UnicodeData.txt",
    "/usr/local/share/ucd/UnicodeData.txt",
    "/usr/share/unicode/UnicodeData.txt",
    "/usr/share/ucd/UnicodeData.txt",
};

struct unicode_entry {
    char buf[1024];
    union {
        char *fields[15];
        struct {
            char *code;
            char *name;
            char *category;
            char *combination_class;
            char *direction;
            char *decomposition;
            char *decimal_value;
            char *digit_value;
            char *numeric_value;
            char *mirrored;
            char *old_name;
            char *comment;
            char *uppercase;
            char *lowercase;
            char *titlecase;
        };
    };
};

struct string_map {
    char const *key;
    char const *value;
};

static bool next_entry(FILE *file, struct unicode_entry *entry) {
    if (fgets(entry->buf, sizeof(entry->buf), file) == NULL) {
        int err = errno;
        if (feof(file)) {
            return false;
        }
        fprintf(stderr, "read error: %s\n", strerror(err));
        return false;
    }
    char *s = entry->buf;
    if (s[strlen(s) - 1] == '\n') {
        s[strlen(s) - 1] = '\0';
    }
    for (size_t i = 0; i < sizeof(entry->fields) / sizeof(entry->fields[0]); i++) {
        entry->fields[i] = s;
        char *c = strchr(s, ';');
        if (c != NULL) {
            c[0] = '\0';
            s = &c[1];
        } else {
            assert(i == sizeof(entry->fields) / sizeof(entry->fields[0]) - 1);
        }
    }
    return true;
}

static int to_utf8(unsigned int code, char s[static 4]) {
    if (code <= 0x7f) {
        s[0] = code;
        return 1;
    } else if (code < 0x800) {
        s[0] = 0xc0 | (code >> 6);
        s[1] = 0x80 | (code & 0x3f);
        return 2;
    } else if (code < 0x10000) {
        s[0] = 0xe0 | (code >> 12);
        s[1] = 0x80 | ((code >> 6) & 0x3f);
        s[2] = 0x80 | (code & 0x3f);
        return 3;
    } else {
        assert(code < 0x200000);
        s[0] = 0xf0 | (code >> 18);
        s[1] = 0x80 | ((code >> 12) & 0x3f);
        s[2] = 0x80 | ((code >> 6) & 0x3f);
        s[3] = 0x80 | (code & 0x3f);
        return 4;
    }
}

static char const *parse_category(char const *category) {
    switch (category[0]) {
        case 'L':
            switch (category[1]) {
                case 'u': return "Letter, Uppercase";
                case 'l': return "Letter, Lowercase";
                case 't': return "Letter, Titlecase";
                case 'm': return "Letter, Modifier";
                case 'o': return "Letter, Other";
            }
            break;
        case 'M':
            switch (category[1]) {
                case 'n': return "Mark, Non-Spacing";
                case 'c': return "Mark, Spacing Combining";
                case 'e': return "Mark, Enclosing";
            }
            break;
        case 'N':
            switch (category[1]) {
                case 'd': return "Number, Decimal Digit";
                case 'l': return "Number, Letter";
                case 'o': return "Number, Other";
            }
            break;
        case 'Z':
            switch (category[1]) {
                case 's': return "Separator, Space";
                case 'l': return "Separator, Line";
                case 'p': return "Separator, Paragraph";
            }
            break;
        case 'C':
            switch (category[1]) {
                case 'c': return "Other, Control";
                case 'f': return "Other, Format";
                case 's': return "Other, Surrogate";
                case 'o': return "Other, Private Use";
                case 'n': return "Other, Not Assigned";
            }
            break;
        case 'P':
            switch (category[1]) {
                case 'c': return "Punctuation, Connector";
                case 'd': return "Punctuation, Dash";
                case 's': return "Punctuation, Open";
                case 'e': return "Punctuation, Close";
                case 'i': return "Punctuation, Initial quote";
                case 'f': return "Punctuation, Final quote";
                case 'o': return "Punctuation, Other";
            }
            break;
        case 'S':
            switch (category[1]) {
                case 'm': return "Symbol, Math";
                case 'c': return "Symbol, Currency";
                case 'k': return "Symbol, Modifier";
                case 'o': return "Symbol, Other";
            }
            break;
    }
    return "Unknown";
}

static char const *parse_direction(char const *direction) {
    if (strcmp(direction, "L") == 0) {
        return "Left-to-Right";
    } else if (strcmp(direction, "R") == 0) {
        return "Right-to-Left";
    } else if (strcmp(direction, "AL") == 0) {
        return "Right-to-Left Arabic";
    } else if (strcmp(direction, "EN") == 0) {
        return "European Number";
    } else if (strcmp(direction, "ES") == 0) {
        return "European Number Separator";
    } else if (strcmp(direction, "ET") == 0) {
        return "European Number Terminator";
    } else if (strcmp(direction, "AN") == 0) {
        return "Arabic Number";
    } else if (strcmp(direction, "CS") == 0) {
        return "Common Number Separator";
    } else if (strcmp(direction, "NSM") == 0) {
        return "Nonspacing Mark";
    } else if (strcmp(direction, "BN") == 0) {
        return "Boundary Neutral";
    } else if (strcmp(direction, "B") == 0) {
        return "Paragraph Separator";
    } else if (strcmp(direction, "S") == 0) {
        return "Segment Separator";
    } else if (strcmp(direction, "WS") == 0) {
        return "Whitespace";
    } else if (strcmp(direction, "ON") == 0) {
        return "Other Neutrals";
    } else if (strcmp(direction, "LRE") == 0) {
        return "Left-to-Right Embedding";
    } else if (strcmp(direction, "LRO") == 0) {
        return "Left-to-Right Override";
    } else if (strcmp(direction, "RLE") == 0) {
        return "Right-to-Left Embedding";
    } else if (strcmp(direction, "RLO") == 0) {
        return "Right-to-Left Override";
    } else if (strcmp(direction, "PDF") == 0) {
        return "Pop Directional Format";
    } else if (strcmp(direction, "LRI") == 0) {
        return "Left-to-Right Isolate";
    } else if (strcmp(direction, "RLI") == 0) {
        return "Right-to-Left Isolate";
    } else if (strcmp(direction, "FSI") == 0) {
        return "First Strong Isolate";
    } else if (strcmp(direction, "PDI") == 0) {
        return "Pop Directional Isolate";
    } else {
        return "Unknown";
    }
}

static const struct string_map decomposition_map[] = {
    { "<font>", "A font variant" },
    { "<noBreak>", "A no-break version of a space or hyphen" },
    { "<initial>", "An initial presentation form" },
    { "<medial>", "A medial presentation form" },
    { "<final>", "A final presentation form" },
    { "<isolated>", "An isolated presentation form" },
    { "<circle>", "An encircled form" },
    { "<super>", "A superscript form" },
    { "<sub>", "A subscript form" },
    { "<vertical>", "A vertical layout presentation form" },
    { "<wide>", "A wide (or zenkaku) compatibility character" },
    { "<narrow>", "A narrow (or hankaku) compatibility character" },
    { "<small>", "A small variant form (CNS compatibility)" },
    { "<square>", "A CJK squared font variant" },
    { "<fraction>", "A vulgar fraction form" },
    { "<compat>", "Otherwise unspecified compatibility character" },
};

static char const *parse_decomposition(char *format, char const *targets[static 8]) {
    char const *decomposition = "Composition";
    char *s = format;
    for (size_t i = 0; i < sizeof(decomposition_map) / sizeof(decomposition_map[0]); i++) {
        size_t len = strlen(decomposition_map[i].key);
        if (strncmp(format, decomposition_map[i].key, len) == 0) {
            decomposition = decomposition_map[i].value;
            s = &format[len];
            break;
        }
    }
    if (s[0] == '\0') {
        targets[0] = NULL;
    } else {
        for (size_t j = 0; j < 7; j++) {
            do { s++; } while (s[0] == ' ');
            targets[j] = s;
            while (s[0] != ' ' && s[0] != '\0') {
                s++;
            }
            if (s[0] == '\0') {
                targets[j+1] = NULL;
                break;
            }
            s[0] = '\0';
        }
    }
    return decomposition;
}

#ifndef _GNU_SOURCE
char *strcasestr(char const *haystack, char const *needle) {
    for (size_t i = 0; haystack[i] != '\0'; i++) {
        size_t j;
        for (j = 0; needle[j] != '\0'; j++) {
            if (tolower(haystack[i+j]) != tolower(needle[j])) {
                break;
            }
        }
        if (needle[j] == '\0') {
            return (char *)&haystack[i];
        }
    }
    return NULL;
}
#endif

static inline bool is_printable(char c)
{
    return c == 'L' || c == 'N' || c == 'P' || c == 'S';
}

int main(int argc, char **argv) {
    const char *search = NULL;
    bool printchr = false;
    enum {
        MATCH_ANY,
        MATCH_WORD_START,
        MATCH_WORD_END,
        MATCH_WORD_WHOLE,
    } match_type = MATCH_ANY;
    int opt;

    while ((opt = getopt(argc, argv, ":dS:s:waeh")) != -1) {
        char const **ptr = NULL;
        switch (opt) {
            case 'D':
                ptr = &ucd_file;
                break;
            case 'S':
                printchr = true;
                /* fall through */
            case 's':
                ptr = &search;
                break;
            case 'w':
            case 'a':
            case 'e':
                if (match_type != MATCH_ANY) {
                    fprintf(stderr, "-%c: match option already specified\n", optopt);
                    return EXIT_FAILURE;
                }
                match_type = opt == 'w' ? MATCH_WORD_WHOLE :
                             opt == 'a' ? MATCH_WORD_START :
                                          MATCH_WORD_END;
                continue;
            case 'h':
                goto help;
            default:
                fprintf(stderr, "-%c: Unknown option\n", optopt);
                return EXIT_FAILURE;
        }
        /* we get here if the option requires a parameter */
        if (optarg == NULL) {
            fprintf(stderr, "-%c: Missing argument\n", optopt);
            return EXIT_FAILURE;
        }
        *ptr = optarg;
    }
    // require exactly zero or one <char> argument, depending on if searching
    if (optind != argc - !search) {
help:
        fprintf(stderr, "Usage: %s [options] [<char>]\n\n"
                        "Options:\n"
                        "  -D <file>  Use <file> as unicode table\n"
                        "  -s <name>  Search for <name> and list matches\n"
                        "  -S <name>  Search for <name> and list matches, showing printable glyphs\n"
                        "  -w         When searching, match only whole words\n"
                        "  -a         When searching, match must begin at the start of a word\n"
                        "  -e         When searching, match must end at the end of a word\n"
                        "  <char>     Unicode code point to display\n"
                        "             Required unless searching\n",
                argv[0]);
        return EXIT_FAILURE;
    }
    const char *match = search ? search : argv[optind];
    const int match_length = match ? strlen(match) : 0;
    struct unicode_entry *entry = malloc(sizeof(struct unicode_entry));
    if (entry == NULL) {
        fprintf(stderr, "Out of memory\n");
        return EXIT_FAILURE;
    }

    FILE *f;
    if (ucd_file != NULL) {
        f = fopen(ucd_file, "r");
        if (f == NULL) {
            int err = errno;
            free(entry);
            fprintf(stderr, "Unable to open UCD file: %s\n", strerror(err));
            return EXIT_FAILURE;
        }
    } else {
        for (size_t i = 0; i < sizeof(default_ucd) / sizeof(default_ucd[0]); i++) {
            f = fopen(default_ucd[i], "r");
            if (f != NULL) {
                break;
            }
        }
        if (f == NULL) {
            free(entry);
            fprintf(stderr, "Unable to open default UCD file\n");
            return EXIT_FAILURE;
        }
    }

    int code = -1;
    if (match[1] != '\0') {
        char *end;
        if (match[0] == 'U' && match[1] == '+') {
            code = strtol(&match[2], &end, 16);
        } else {
            code = strtol(match, &end, 16);
        }
        if (end[0] != '\0') {
            code = -1;
        }
    }
    bool any_matches = false;
    while (next_entry(f, entry)) {
        bool matches = false;
        unsigned int entry_code;
        sscanf(entry->code, "%x", &entry_code);
        if (code != -1) {
            matches = (entry_code == (unsigned int)code);
        }

        if (search != NULL) {
            const char *matching = strcasestr(entry->name, match);
            if (matching) {
                if      ((match_type & MATCH_WORD_START) && (matching == match || isalpha(matching[-1])))
                    matching = NULL;
                else if ((match_type & MATCH_WORD_END)   && isalpha(matching[match_length]))
                    matching = NULL;
                matches |= !!matching;
            }
        }

        char c[5];
        int l = to_utf8(entry_code, c);
        c[l] = '\0';
        matches |= strcmp(c, match) == 0;
        if (matches) {
            any_matches = true;
            if (printchr && is_printable(entry->category[0]))
                printf("U+%s: %s %s\n", entry->code, c, entry->name);
            else
                printf("U+%s: %s\n", entry->code, entry->name);
            if (search != NULL) {
                continue;
            }

            if (is_printable(entry->category[0])) {
                printf("  Character: %s\n", c);
            }
            printf("  Category: %s\n", parse_category(entry->category));
            printf("  Text direction: %s\n", parse_direction(entry->direction));
            if (entry->decomposition[0] != '\0') {
                char const *targets[8];
                char const *desc = parse_decomposition(entry->decomposition,
                    targets);
                printf("  Decomposition: %s", desc);
                if (targets[0] != NULL) {
                    printf(" of U+%s", targets[0]);
                    for (size_t i = 1; targets[i] != NULL; i++) {
                        printf(" and U+%s", targets[i]);
                    }
                }
                printf("\n");
            }
            if (entry->decimal_value[0] != '\0') {
                printf("  Decimal digit: %s\n", entry->decimal_value);
            }
            if (entry->digit_value[0] != '\0') {
                printf("  Digit: %s\n", entry->digit_value);
            }
            if (entry->numeric_value[0] != '\0') {
                printf("  Numeric value: %s\n", entry->numeric_value);
            }
            if (entry->mirrored[0] == 'Y') {
                printf("  Mirrored");
            }
            if (entry->old_name[0] != '\0') {
                printf("  Old name: %s\n", entry->old_name);
            }
            if (entry->uppercase[0] != '\0') {
                printf("  Uppercase equivalent: U+%s\n", entry->uppercase);
            }
            if (entry->lowercase[0] != '\0') {
                printf("  Lowercase equivalent: U+%s\n", entry->lowercase);
            }
            if (entry->titlecase[0] != '\0') {
                printf("  Titlecase equivalent: U+%s\n", entry->titlecase);
            }
            break;
        }
    }

    if (!any_matches) {
        printf("No entry matching %s\n", match);
    }
    fclose(f);
    free(entry);
    return EXIT_SUCCESS;
}
